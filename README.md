# Goldstandard für Gewitter- und Hagerereignisse

Der Goldstandard umfasst Daten zu Gewitter- und Hagelereignissen, die mithilfe der in der Dissertation "Voraussetzungen und Grenzen der Auswertung klimarelevanter Informationen historischer Textquellen mit Hilfe von Automatisierungsprozessen" entwickelten Methode aus Textquellen von HISKLID2 ausgewertet wurden [DOI 10.6094/UNIFR/244079](https://doi.org/10.6094/UNIFR/244079).

Die Daten liegen in zwei separaten CSV-Dateien vor: 

1. Quellen in `GOLD_hisklid2_source_hundert_hail_v<Version>.csv`.
2. Zitate und deren Auswertung: `GOLD_hisklid2_hail_thunder_events_v<Version>_<Datum>.csv`.

Die Daten enthalten auch die Originaldaten aus HISKLID2 (englische Spalten).

# Gold standard for thunderstorm and hail events

The gold standard comprises data on thunderstorm and hail events that were analysed from HISKLID2 text sources using the method developed in the dissertation "Voraussetzungen und Grenzen der Auswertung klimarelevanter Informationen historischer Textquellen mit Hilfe von Automatisierungsprozessen" [DOI 10.6094/UNIFR/244079](https://doi.org/10.6094/UNIFR/244079).

The data are available in two separate CSV files: 

1. sources in `GOLD_hisklid2_source_hundred_hail_v<version>.csv`.
2. quotations and their evaluation: `GOLD_hisklid2_hail_thunder_events_v<version>_<date>.csv`.

The data also contains the original data from HISKLID2 (English columns).


## Warum auf sind die Spaltennamen auf deutsch? Why are the column names in German?

Die Originalzitate sind in Latein und den deutschen Sprachstufen Mittelhochdeutsch, Frühneuhochdeutsch, Neuhochdeutsch und Gegenwartsdeutsch verfasst. Um die Klimainformationen aus den Zitaten zu extrahieren, ist daher ein Verständnis der deutschen Sprache erforderlich. Zur Kennzeichnung der Spalten, die mit dem Arbeitsablauf erstellt wurden, wurden diese im Vergleich zum Originaldatensatz deutsche Namen verwendet.

The original quotations are written in Latin and the German language levels Middle High German, Early New High German, Modern High German and Contemporary German. An understanding of the German language is therefore required in order to extract the climate information from the quotations. To identify the columns that were created with the workflow, German names were used in comparison to the original data set.


## Quellenangaben

***Originaldaten aus HISKLID2***
|Spaltenname   | Beschreibung  |
|--------------|---------------|
|source_id          | Eindeutige Identifikationsnummer  |
|source_title       | Titel     |
|source_author      | Autor   |
|publication_date   | Datum der Veröffentlichung  |
|source_year        | Jahr der Veröffentlichung  |
|source_publisher   | Herausgeber  |
|source_signature   | Signatur   |
|institution        | Aufbewahrungsort  |
|note               | Notiz    |
|volume             |Band   |
|url                | Entfernte elektronische Adresse
| original_data     | Ursprüngliche Daten in HISKLID


***Goldstandard***
|Spaltenname   | Beschreibung  |
|--------------|---------------|
| nachname     |  Nachname des oder der Mitwirkenden |
| vorname      | Vorname des oder der Mitwirkenden |
| geboren      | Geburtsdatum des oder der Mitwirkenden |
| gestorben    |  Sterbedatum des oder der Mitwirkenden |
| wirkungsort  |  Ort an welcher der oder die Mitwirkende tätig war, kommagetrenntes Multifeld | 
| wirkungsdaten |  Zeitangaben zum Wirkungsort, kommagetrenntes Multifeld | 
| beruf |  Berufe des oder der Mitwirkenden, kommagetrenntes Multifeld| 
| biographie_url | Entfernte URL-Adresse mit biographische Nachweisen| 
| gnd_url |  Entfernte URL-Adresse mit biographische Nachweisen der GND| 
| religion |  Konfession des oder der Mitwirkenden, kommagetrenntes Multifeld| 
| veröffentlichungsjahr | Jahr der Veröffentlichung der Textquelle| 
| beteiligt |  Nachname, Vorname des oder der Mitwirkenden| 
| beteiligt_typ |  Art der Beteiligung: Drucker, Verfasser, Bearbeiter, Herausgeber oder ohne Angaben (o.A)| 
| sprache: Textsprache |  Mittelhochdeutsch, Frühneuhochdeusch, Neuhochdeutsch, Gegenwartsdeutsch| 
| quellentyp |  Chronik, Verwaltungsakten, Schiffsjournale, Geschichtsschreibung, Tagebuch, Zeitung, Zeitschrift, Jahrbuch (Annalen), Buchhaltung, Kompilation, Wetteraufzeichnungen, unbekannt| 
| titel  |  Titel der Quelle| 
| titel_ort  |  Ortsangaben im Titel| 
| titel\_ort\_breitengrad  |  Breitengrade der Ortsangaben| 
| titel\_ort\_längengrad |  Längengrade der Orstangaben| 
| provenienz  |  Herkunft der Textquelle| 
| provenienz_breitengrad  |  Breitengrade der Herkunft der Textquelle| 
| provenienz_längengrad  |  Längengrade der Herkunft der Textquelle| 
| quellen_url |  Digitaler Nachweis der Textquelle| 
| digitalisat_url |  Digitalisat der Quelle| 
| bemerkung |  Bemerkungsfeld| 
| kalender |  Angaben zum verwendeten Kalendersystem| 
| kalender_typ |  greogorianisch, julianisch| 
| authentizität |  Orginal, Abschrift, kA (keine Angaben möglich)| 
| texterzeugnis |  Druck, Handschrift, kA (keine Angaben möglich)| 
| zustand |  1,2,3,4, kA (keine Angaben möglich)| 
| material |  Digitalisat, Papier (keine Angaben möglich)| 
| ereignis_anzahl |  Anzahl der Gewitter- oder Hagelereignisse in dieser Textquelle| 
| intention |  dokumentierend, erbauend, informativ, selbstreferentiell| 
| notiz |  Annotation oder Hinweise zur Textquelle| 

## Zitate

***Originaldaten aus HISKLID2***
|Spaltenname   | Beschreibung  |
|--------------|---------------|
| source_id |  Eindeutige Identifikationsnummer der Textquelle| 
| quote_id |  Eindeutige Identifikationsnummer des Zitats| 
| quote_text |  transkribierter Textausschnitt| 
| time_begins |  Datumsangaben zum Beginn der Ereignisse| 
| time_ends |  Datumsangaben zum Ende der Ereignisse| 
| locations_names |  Ortsangaben zum Ereignis| 
| latitudes |  Breitengrade zu den Ortsangaben| 
| longitudes |  Längengrade zu den Ortsangaben| 
| node_ids |  eindeutige Indentifikationsnummern der Ereignistypen (Kodierungsschema in Tambora)| 
| node_labels |  englische Bezeichnungen der Ereignistypen| 

***Goldstandard***
|Spaltenname   | Beschreibung  |
|--------------|---------------|
|zitat_id| Eindeutige Identifikationsnummer des Zitats |
|zitat_normalisiert| sprachlich normalisiertes Zitat (Gegenwartsdeutsch) |
|sprache| deutsche Sprachstufe oder Sprache des Zitats |
|gewitter_anzahl| Anzahl der im Text genannten Gewitterereignisse |
|hagel_anzahl| Anzahl der im Text genannten Hagelereignisse |
|heilig_kirch_alt_datum| Liegt das Datum als Heiligentag oder kirchliches Fest oder in anderer nicht numerischer Form vor? |
|kalender_typ| greogorianisch, julianisch, Mondkalender |
|ereignis_beginn| Datumsangaben zum Beginn der Ereignisse |
|ereignis_ende| Datumsangaben zum Ende der Ereignisse |
|ort| Ortsangaben zum Ereignis |
|ort_breitengrad| Breitengrade zu den Ortsangaben |
|ort_laengengrad| Längengrade zu den Ortsangaben |
|ereignis| englische Bezeichnungen der Ereignistypen |
|gewitter| Wird im Text ein Gewitter beschrieben? |
|gewitter_klasse| Gewitterklasse |
|hagel| Wird im Text Hagel beschrieben? |
|hagel_klasse| Hagelklasse |
|schnee| Wird im Text Schnee im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|schnee_klasse| Schneeklasse |
|regen| Wird im Text Regen im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|regen_klasse| Regenklasse |
|wind| Wird im Text Wind im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|wind_klasse| Windklasse |
|blitzschlag| Wird im Text ein Blitzeinschlag beschrieben? |
|ueberschwemmung| Wird im Text eine Überschwemmung im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|ueberschwemmung_klasse| Überschwemmungsklasse |
|tornado| Wird im Text ein Tornado beschrieben? |
|todesfall| Wird im Text ein Todesfall im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|todesfall_klasse| Todesfallklasse |
|schaden| Werden im Text Schäden im Zusammenhang mit dem Gewitter- oder Hagelereignis beschrieben? |
|schaden_klasse| Schadensklasse |
|veröffentlichungsjahr| Jahr der Veröffentlichung der Textquelle |
|sprache_quelle| Sprache oder Sprachstufe, in welcher die Textquelle verfasst wurde |
|titel| Titel der Textquelle |
|titel\_ort| Ortsangaben im Titel |
|titel\_ort\_breitengrad| Breitengrade der Ortsangaben |
|titel\_ort\_längengrad| Längengrade der Orstangaben |
|provenienz| Herkunft der Textquelle |
|provenienz_breitengrad| Breitengrade der Herkunft der Textquelle |
|provenienz_längengrad| Längengrade der Herkunft der Textquelle |
|quellenzuordnung| Zitate die potentiell das selbe Ereignis beschreiben: eindeutige Kennziffer der Zitate |
|anzahl_zuordnung| Anzahl der Zitate, die poteniell das selbe Ereignis beschreiben. |
|uwz_klasse| Plausibilitätsklasse |
|intention| dokumentierend, erbauend, informativ, selbstreferentiell |
|textzeugnis| Textzeugnis, Zeitzeuge oder Augenzeuge |

## Was bdeutet Goldstandard?
Goldstandard bezeichnet immer den aktuellsten Datensatz, der sich von vorherigen Datensätzen durch Fehlerbehebungen oder die Anreicherung weiterer Merkmale unterscheidet. Es handelt sich dabei jedoch keineswegs um einen unveränderlichen und fehlerfreien Datensatz. Es handlet sich also um eine Weiterentwicklung des bereits hochwertigen Datenauszugs aus HISKLID2.


## Lizenz
Der Goldstandard steht unter einer CC BY-SA 4.0 Lizenz zur Verfügung.
[https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

## Licence
Goldstandard - HISKLID2 © 2024 by Franck Schätz is licensed under CC BY-SA 4.0. To view a copy of this license, visit [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

